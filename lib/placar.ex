defmodule Placar do
  require Armazenamento

  @moduledoc """
  Documentation for Placar.
  """

  @doc """
  Placar.

  ## Examples

      iex> Placar.ponto("guerra","100","caipirinha")
      :ok

  """

  def ponto(usuario, ponto, tipo) do
    Armazenamento.ponto(usuario, ponto, tipo)
  end

  @spec todos_pontos(any) :: [any]
  def todos_pontos(usuario) do
    Armazenamento.pontos_registrados()
    |> Enum.map(fn tipo ->
      %{tipo => Integer.to_string(Armazenamento.ponto_usuario(usuario, tipo))}
    end)
  end

  def ranking(tipo) do
    Armazenamento.usuarios_com_ponto()
    |> Enum.map(fn usuario ->
      {String.to_atom(usuario), Armazenamento.ponto_usuario(usuario, tipo)}
    end)
    |> List.keysort(1)
    |> Enum.reverse()
  end
end
