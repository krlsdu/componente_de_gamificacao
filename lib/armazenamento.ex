defmodule Armazenamento do
  @moduledoc """
  Documentation for Armzenamento.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Armazenamento.ponto("guerra","30","estrela")
      :ok

  """
  @enforce_keys [:usuario, :ponto, :tipo]
  @derive [Jason.Encoder]
  defstruct [:usuario, :ponto, :tipo]

  @spec ponto(String.t(), String.t(), String.t()) :: :ok
  def ponto(usuario, ponto, tipo) do
    ponto = %Armazenamento{usuario: usuario, ponto: ponto, tipo: tipo}
    registro = Jason.encode!(ponto)
    File.write!("/tmp/armazenamento.txt", "#{registro}\n", [:append])
  end

  @spec ponto_usuario(String.t(), String.t()) :: any
  def ponto_usuario(usuario, tipo) do
    File.stream!("/tmp/armazenamento.txt")
    |> Stream.map(fn x -> Jason.decode!(x) end)
    |> Stream.filter(fn event ->
      Map.take(event, ["usuario", "tipo"]) == %{"tipo" => tipo, "usuario" => usuario}
    end)
    |> Stream.map(fn ponto -> Map.get(ponto, "ponto") end)
    |> Enum.reduce(0, fn x, acc -> String.to_integer(x) + acc end)
  end

  @spec usuarios_com_ponto :: MapSet.t(any)
  def usuarios_com_ponto() do
    File.stream!("/tmp/armazenamento.txt")
    |> Stream.map(fn x -> Jason.decode!(x) end)
    |> Stream.map(fn x -> Map.get(x, "usuario") end)
    |> MapSet.new()
  end

  @spec pontos_registrados :: MapSet.t(any)
  def pontos_registrados() do
    File.stream!("/tmp/armazenamento.txt")
    |> Stream.map(fn x -> Jason.decode!(x) end)
    |> Stream.map(fn x -> Map.get(x, "tipo") end)
    |> MapSet.new()
  end
end
