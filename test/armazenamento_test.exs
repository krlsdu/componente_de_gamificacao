defmodule ArmazenamentoTest do
  use ExUnit.Case
  doctest Armazenamento

  setup do
    if File.exists?("/tmp/armazenamento.txt") do
      File.rm("/tmp/armazenamento.txt")
    end
  end

  setup do
    Armazenamento.ponto("guerra", "40", "estrela")
    Armazenamento.ponto("guerra", "10", "curtida")
    Armazenamento.ponto("guerra", "40", "estrela")
    Armazenamento.ponto("guerra", "20", "fervida")
    Armazenamento.ponto("marcia", "60", "estrela")
    Armazenamento.ponto("elena", "50", "eleninhas")
    Armazenamento.ponto("joao", "10", "curtida")
    Armazenamento.ponto("kirito", "20", "fervida")
    Armazenamento.ponto("marcia", "60", "estrela")
    Armazenamento.ponto("elena", "50", "eleninhas")
    :ok
  end

  test "Armazenar que um usuário recebeu uma quantidade de um tipo de ponto" do
    assert Armazenamento.ponto("guerra", "40", "estrela") == :ok

    registros_lido =
      File.stream!("/tmp/armazenamento.txt") |> Enum.map(fn x -> Jason.decode!(x) end)

    dado_esperado = %{"usuario" => "guerra", "ponto" => "40", "tipo" => "estrela"}
    assert Enum.any?(registros_lido, fn registro -> registro == dado_esperado end)
  end

  test "quantos pontos de um tipo tem um usuário" do
    pontos_esperado = 80
    assert Armazenamento.ponto_usuario("guerra", "estrela") == pontos_esperado
  end

  test "usuários que já receberam algum tipo de ponto" do
    usuarios_esperados = MapSet.new(["guerra", "elena", "marcia", "kirito", "joao"])
    assert Armazenamento.usuarios_com_ponto() == usuarios_esperados
  end

  test "tipos de ponto que já foram registrados para algum usuário" do
    tipos_esperados = MapSet.new(["estrela", "curtida", "fervida", "eleninhas"])
    assert Armazenamento.pontos_registrados() == tipos_esperados
  end
end
