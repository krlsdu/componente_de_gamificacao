defmodule PlacarTest do
  use ExUnit.Case
  doctest Placar

  setup do
    if File.exists?("/tmp/armazenamento.txt") do
      File.rm("/tmp/armazenamento.txt")
    end
  end

  test "Registrar um tipo de ponto para um usuário" do
    assert Placar.ponto("guerra", "100", "caipirinha") == :ok
  end

  test "Retornar todos os pontos de um usuário" do
    Placar.ponto("guerra", "100", "caipirinha")
    Placar.ponto("guerra", "10", "estrela")
    assert Placar.todos_pontos("guerra") == [%{"caipirinha" => "100"}, %{"estrela" => "10"}]
  end

  test "ranking" do
    Placar.ponto("guerra", "100", "estrela")
    Placar.ponto("carlos", "50", "estrela")
    Placar.ponto("ferrari", "10", "estrela")
    Placar.ponto("alexandre", "500", "estrela")

    assert Placar.ranking("estrela") == [alexandre: 500, guerra: 100, carlos: 50, ferrari: 10]
    I
  end
end
